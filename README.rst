MongoDB Atlas Cluster
~~~~~~~~~~~~~~~~~~~~~

*MFlix* uses *MongoDB* to persist all its data.

One of the easiest ways to get up and running with MongoDB is to use *MongoDB Atlas*,
a hosted and fully-managed database solution.

If you have taken other MongoDB University courses like M001 or M121, you may
already have an account - feel free to reuse that cluster for this course.

Make sure to use a **free tier cluster** for the application and course.

*Note: Be advised that some of the UI aspects of Atlas may have changed since
the redaction of this README, therefore some of the screenshots in this file may
be different from the actual Atlas UI interface.*


Using an existing MongoDB Atlas Account:
****************************************

If you already have a previous *Atlas* account created, perhaps because you've
taken one of our other MongoDB university courses, you can repurpose it for
M220J.

Log into your *Atlas* account and create a new project named **M220** by clicking
on the *Context* dropdown menu:

.. image:: https://s3.amazonaws.com/university-courses/m220/cluster_create_project.png

After creating this new project, skip the next section and proceed to the
*Creating an M0 free tier cluster mflix* section.


Creating a new MongoDB Atlas Account:
*************************************

If you do not have an existing *Atlas* account, go ahead and `create an Atlas
Account <https://cloud.mongodb.com/links/registerForAtlas>`_ by filling in the
required fields:

.. image:: https://s3.amazonaws.com/university-courses/m220/atlas_registration.png


Creating an M0 free tier cluster **mflix**:
*******************************************

*Note: You will need to do this step even if you are reusing an Atlas account.*

1. After creating a new project, you will be prompted to create the first
   cluster in that project:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_create.png


2. Choose AWS as the cloud provider, in a Region that has the label
   **Free Tier Available**:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_provider.png


3. Select *Cluster Tier* **M0**:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_tier.png


4. Set *Cluster Name* to **mflix** by clicking on the default name
   *Cluster0*, and click *Create Cluster*:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_name.png


5. Once you press *Create Cluster*, you will be redirected to the account
   dashboard. In this dashboard, make sure that the project is named **M220**.
   If not, go to the *Settings* menu item and change the project name
   from the default *Project 0* to **M220**:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_project.png


6. Next, configure the security settings of this cluster, by enabling the *IP
   Whitelist* and *MongoDB Users*:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_ipwhitelisting.png

  Update your IP Whitelist so that your app can talk to the cluster. Click the
  "Security" tab from the "Clusters" page. Then click "IP Whitelist" followed by
  "Add IP Address". Finally, click "Allow Access from Anywhere" and click
  "Confirm".

  *Note that in a production environment, you would control very tightly the list of
  IP addresses that can connect to your cluster.*

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_allowall.png


7. Then create the MongoDB database user required for this course:

  - username: **m220student**
  - password: **m220password**

  You can create new users through *Security* -> *MongoDB Users* -> *Add New User*

  Allow this user the privilege to **Read and write to any database**:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_application_user.png


8. When the user is created, and the cluster is deployed, you can test the setup
   by connecting via the ``mongo`` shell. You can find instructions to connect
   in the *Connect* section of the cluster dashboard:

  .. image:: https://s3.amazonaws.com/university-courses/m220/cluster_connect_application.png

  Go to your cluster *Overview*  -> *Connect* -> *Connect Your Application*.
  Select the option corresponding to MongoDB version3.6+ and copy the
  ``mongo`` connection URI.

  The below example connects to *Atlas* as the user you created before, with
  username **m220student** and password **m220password**. You can run this command
  from your command line:

  .. code-block:: sh

    mongo "mongodb+srv://m220student:m220password@<YOUR_CLUSTER_URI>"

  By connecting to the server from your host machine, you have validated that the
  cluster is configured and reachable from your local workstation.
